package com.emsrepo.service;

import java.util.List;

import com.emsrepo.entity.Logger;
import com.emsrepo.vo.LoggerVO;

public interface LoggerService {

	public List<Logger> getLoggerDetailList();
}
